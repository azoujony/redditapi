# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.
import requests
import pandas as pd
import openpyxl
import time
import Abbreviation_dict
import enchant


def get_posts_as_DF(Subreddit_list, NSFW_Tag):
    # Use a breakpoint in the code line below to debug your script.

    CLIENT_ID = 'iugfJkTl9b1nhmOxFOlBaw'
    SECRET_KEY = 'U973NP5YDpvGwN43hbtLf7xHr1goQw'
    pw = '1j2r3y45'

    auth = requests.auth.HTTPBasicAuth(CLIENT_ID, SECRET_KEY)
    data = {'grant_type': 'password',
            'username': 'jonathan6661',
            'password': pw}

    headers = {'User-Agent': 'MyBot/0.0.1'}
    res = requests.post('https://www.reddit.com/api/v1/access_token',
                        auth=auth, data=data, headers=headers)

    TOKEN = res.json()['access_token']
    headers['Authorization'] = f'bearer {TOKEN}'

    df = pd.DataFrame()  # initialize dataframe

    for Subreddit in Subreddit_list:
        res = requests.get(f"https://oauth.reddit.com/r/{Subreddit}/new", headers=headers)

        # loop through each post retrieved from GET request
        if NSFW_Tag is None:
            for post in res.json()['data']['children']:
                # append relevant data to dataframe
                df = df._append({
                    # 'subreddit': post['data']['subreddit'],
                    'title': post['data']['title'],
                    'selftext': post['data']['selftext'],
                    # 'upvote_ratio': post['data']['upvote_ratio'],
                    # 'ups': post['data']['ups'],
                    # 'downs': post['data']['downs'],
                    # 'score': post['data']['score']
                    'NSFW': post['data']['over_18']
                }, ignore_index=True)
        else:
            for post in res.json()['data']['children']:
                if post['data']['over_18'] == NSFW_Tag:
                    # append relevant data to dataframe
                    df = df._append({
                        # 'subreddit': post['data']['subreddit'],
                        'title': post['data']['title'],
                        'selftext': post['data']['selftext'],
                        # 'upvote_ratio': post['data']['upvote_ratio'],
                        # 'ups': post['data']['ups'],
                        # 'downs': post['data']['downs'],
                        # 'score': post['data']['score']
                        'NSFW': post['data']['over_18']
                    }, ignore_index=True)

    print(df)
    return df


def convert_DF_to_export_file(df, output_file):
    # Save the DataFrame to Excel
    df.to_excel(output_file, index=False)  # Set index=False to exclude index column

    # # Replace 'output_file.txt' with the desired name for the output TXT file
    # output_file = f'{Project_name}.txt'

    # Save DataFrame as a tab-separated TXT file
    df.to_csv(output_file, sep='\t', index=False)


def organize_file_to_format(df):
    Formated_df = df
    for Text in df['selftext']:
        Formated_df['selftext'] = fix_grammar(Text, Abbreviation_dict)
    output_file = f'/Users/yehonatanazoulay/PycharmProjects/Reddit API/Formated {Project_name}.xlsx'
    convert_DF_to_export_file(Formated_df, output_file)
    return None


def fix_grammar(text, dictionary_name='en_US'):
    checker = enchant.Dict(dictionary_name)

    words = text.split()
    corrected_words = []

    for word in words:
        corrected_word = checker.suggest(word)
        if corrected_word:
            corrected_words.append(corrected_word[0])
        else:
            corrected_words.append(word)

    corrected_text = ' '.join(corrected_words)
    return corrected_text


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    Project_name = 'Reddit Posts' + ' ' + time.strftime("%d%m%Y")
    print(Project_name)
    output_file = f'/Users/yehonatanazoulay/PycharmProjects/Reddit API/{Project_name}.xlsx'
    Subreddit_list = ('offmychest', 'relationships', 'tifu')  # Order dictates priority
    NSFW_Tag = False  # None:include NSFW and not NSFW posts ,True: include only NSFW posts ,False: exclude NSFW posts
    df = get_posts_as_DF(Subreddit_list, NSFW_Tag)
    convert_DF_to_export_file(df, output_file)
    organize_file_to_format(df)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
